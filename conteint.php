<?php 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="Stylesheets/home.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <title>MakerMan</title>
</head>

<body id="textover">
    <nav class="navbar navbar-expand navbar-fixed-top navbar-dark bg-primary">
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <div class="navbar-nav">
                <div class="navbar-nav mr auto dropdown nav-logo ">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="logo">
                            <img src="images/2000px-Adidas_Logo.svg.png" height="20" alt="">
                        </div>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" id="bootover">
                        <a class="dropdown-item" href="#">3D Printing</a>
                        <a class="dropdown-item" href="#">RC Planes</a>
                        <a class="dropdown-item" href="#">RC Quads</a>
                        <a class="dropdown-item" href="#">Arduino</a>
                        <a class="dropdown-item" href="#">Robotics</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="Home.html">Home page</a>
                        <a class="dropdown-item" href="#">About Us</a>
                        <a class="dropdown-item" href="#">FAQ Page</a>
                        <a class="dropdown-item" href="#">Admin Aplication</a>
                        <a class="dropdown-item" href="#">Policy Documents</a>
                        <a class="dropdown-item" href="#">Home page</a>
                        <a class="dropdown-item" href="#">Home page</a>
                    </div>
                </div>
            </div>
            <div class="nav-item mx-auto">
                <a class="name" href="Home.html">MakerForge</a>
            </div>
            <div class="navbar-nav ml-auto">
                <a class="btn btn-success" href="#" role="button">Login</a>
            </div>
        </div>
    </nav>
    <div class="subthreadlink container-flex border border-dark">
        <div class="row" id="textcolourover">
            <a class="col" href="#">eiusmod </a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
            <a class="col" href="#">eiusmod</a>
        </div>
    </div>


    <div class="cont-head">
        <H4>Sed ut perspiciatis und e omnis iste na</H4>
    </div>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-9" role="main">
                <div class="carousel slide" id="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="images/p (1).JPG" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img src="images/p (2).JPG" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img src="images/p (3).JPG" alt="Third slide">
                        </div>
                    </div>

                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                </div>
                <div class="text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a dui luctus, hendrerit lectus id, congue ligula. Ut dignissim, ex non egestas volutpat, leo lorem vehicula ex, eget lobortis eros ipsum non neque. Vivamus sodales pellentesque est id rutrum. Morbi molestie feugiat massa, at finibus urna venenatis in. Cras sit amet ultricies massa. Donec leo magna, consectetur ac libero sollicitudin, commodo aliquam lectus. Donec commodo aliquam vehicula. Proin tempor, nunc vel mattis cursus, turpis neque congue elit, sit amet dignissim magna nunc sed mi. In leo libero, volutpat sed suscipit et, dignissim quis enim. Nullam quis lacus dictum, tempor mi ut, sagittis arcu. Aliquam id nisi quis orci volutpat elementum. Proin id nulla ex. Donec molestie placerat porttitor.</p>
                </div>
            </div>
            <div class=""></div>
            <div class="col-md-3">
                <div class="profile border border-dark rounded">
                    <img src="images/p (2).JPG" alt="" class="userimg">
                    <div class="userinfo" id="info">
                        <p class="h4"> profile</p>
                        <ul>
                            <li>iste na</li>
                            <li>iste na</li>
                            <li>iste na</li>
                        </ul>
                    </div>
                </div>
                <div class="downloads border border-dark rounded" id="info">
                    <p class="h4">downloads</p>
                    <div class="downlinks border border-dark rounded">
                        <ul>
                            <li><a href="#">iste na</a></li>
                            <li><a href="#">iste na</a></li>
                            <li><a href="#">iste na</a></li>
                            <li><a href="#">iste na</a></li>
                            <li><a href="#">iste na</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>