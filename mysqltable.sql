USE `container-db`;

DROP TABLE IF EXISTS tbl_user;
DROP TABLE IF EXISTS tbl_imgtable;
DROP TABLE IF EXISTS tbl_posts;


CREATE TABLE tbl_user (

    ID INT(255) NOT NULL AUTO_INCREMENT,
    FNAME VARCHAR(255) NOT NULL,
    LNAME VARCHAR(255),
    UNAME VARCHAR(255) NOT NULL,
    PASS VARCHAR(255) NOT NULL,
    POSTNO INT(255),
    DATE ,
    COMENTSNO INT(255),

    PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

CREATE TABLE tbl_imgtable (

    IMGID INT(255) NOT NULL AUTO_INCREMENT,
    IMG VARCHAR(255),
    IPOSTID INT(255) NOT NULL,
    PRIMARY KEY (IMGID),
    FOREIGN KEY (IPOSTID) REFERENCES tbl_posts(POSTID)
) AUTO_INCREMENT=1;
CREATE TABLE tbl_posts (

    POSTID INT(255) NOT NULL AUTO_INCREMENT,
    U-ID INT(255) NOT NULL AUTO_INCREMENT,
    UNAME VARCHAR(255) NOT NULL,
    PCOUNTER INT(255),
    THUMBNAIL VARCHAR(255),
    PRIMARY KEY (POSTID),
    FOREIGN KEY (U-ID) REFERENCES tbl_user(ID),

) AUTO_INCREMENT=1;

INSERT INTO tbl_user (FNAME,LNAME,UNAME,PASS,POSTNO) VALUES ('Jeff', 'Kranenburg', 'awesomeguy', 'second' ,5);
INSERT INTO tbl_user (FNAME,LNAME,UNAME,PASS,POSTNO) VALUES ('Me', 'Good', 'Grades', 'sandman' ,5);
INSERT INTO tbl_user (FNAME,LNAME,UNAME,PASS,POSTNO) VALUES ('Nick', 'Bunce', 'Hope', 'getit' , 5);
INSERT INTO tbl_user (FNAME,LNAME,UNAME,PASS,POSTNO) VALUES ('Sam', 'Smith', 'builer40','16charcters' ,5);
INSERT INTO tbl_user (FNAME,LNAME,UNAME,PASS,POSTNO) VALUES ('John', 'Smith', 'You','dorwssap' ,8);
INSERT INTO tbl_user (FNAME,LNAME,UNAME,PASS,POSTNO) VALUES ('Fred', 'Lee', 'Can', '123456789' ,7);
INSERT INTO tbl_user (FNAME,LNAME,UNAME,PASS,POSTNO) VALUES ('Bob', 'Lee', 'See', 'hello', 5);



SELECT * FROM   tbl_user;


