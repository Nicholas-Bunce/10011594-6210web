<?php 
$name = "Bob"; //this is now a string
echo gettype($name);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link rel="stylesheet" href="Stylesheets/home.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
  <title>MakerMan</title>
</head>

<body id="textover">
  <nav class="navbar navbar-expand navbar-fixed-top navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <div class="navbar-nav">
        <div class="navbar-nav mr auto dropdown nav-logo ">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="logo">
              <img src="images/2000px-Adidas_Logo.svg.png" height="20" alt="">
            </div>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" id="bootover">
            <a class="dropdown-item" href="conteint.html">3D Printing</a>
            <a class="dropdown-item" href="#">RC Planes</a>
            <a class="dropdown-item" href="#">RC Quads</a>
            <a class="dropdown-item" href="#">Arduino</a>
            <a class="dropdown-item" href="#">Robotics</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="Home.html">Home page</a>
            <a class="dropdown-item" href="#">About Us</a>
            <a class="dropdown-item" href="#">FAQ Page</a>
            <a class="dropdown-item" href="#">Admin Aplication</a>
            <a class="dropdown-item" href="#">Policy Documents</a>
            <a class="dropdown-item" href="#">Home page</a>
            <a class="dropdown-item" href="#">Home page</a>
          </div>
        </div>
      </div>
      <div class="nav-item mx-auto">
        <a class="name" href="Home.html">MakerForge</a>
      </div>
      <div class="navbar-nav ml-auto">
        <a class="btn btn-success" href="#" role="button">Login</a>
      </div>
    </div>
  </nav>
  <div class="subthreadlink container-flex">
    <div class="row" id="textcolourover">
      <a class="col" href="#">eiusmod </a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
      <a class="col" href="#">eiusmod</a>
    </div>
  </div>
  <a href="#" id="textcolourover">
    <div class="container">
      <div class="row">
        <div class="col">
          <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit
            in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
            sunt in culpa </p>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).jpg" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="post-tab col-xs-8 border border-dark rounded">
          <div class="row">
            <div class="col ptab-img">
              <img src="images/p (1).JPG" alt="" class="rounded">
            </div>
            <div class="pdecription col-9">
              <p> ipsum dolor sit amet,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa </p>
            </div>
            <div class="pcounter border border-danger rounded col"></div>
          </div>
        </div>
      </div>
    </div>
  </a>

</body>

</html>